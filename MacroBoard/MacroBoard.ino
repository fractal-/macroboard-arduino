
/* 
   Macro Board
   Made by
   Jeremie GABOLDE
*/   


/*
 * saving presets in eeprom
 */
#include <EEPROM.h>

int memAdd = 0;

int memValue;


/*
 * RotaryEncoder lib
 */
#include <SimpleRotary.h>


/*
  RawHID
 */

#define VENDOR_ID               0x16C0
#define PRODUCT_ID              0x0486
#define RAWHID_USAGE_PAGE       0xFFAB  // recommended: 0xFF00 to 0xFFFF
#define RAWHID_USAGE            0x0200  // recommended: 0x0100 to 0xFFFF

byte buffer[64];
int HIDIn;
String message;

/*
 * time managment
 */
int lastCDMillis = 0;
int coolDown = 50;

/*
  PINS
*/

// LEDS
const int LED_blink = 13;

// BUTTONS Rotary
const int BUTTON_Rotary_Left = 12;
bool BUTTON_Rotary_Left_lastState = HIGH;
const int BUTTON_Rotary_Middle = 9;
bool BUTTON_Rotary_Middle_lastState = HIGH;;
const int BUTTON_Rotary_Right = 6;
bool BUTTON_Rotary_Right_lastState = HIGH;;

//SHIFTIN
const int latchPin = 3;
const int clockPin = 2;
const int dataPin = 1;

byte shiftinValue = 72;  //01001000 avoid bug

//BUTTONS ON SHIFTIN
const byte BUTTON_Toggle_Left = 2;
const byte BUTTON_Toggle_Middle = 3;
const byte BUTTON_Toggle_Right = 1;
const byte BUTTON_Push_Left = 6;
const byte BUTTON_Push_Middle = 5;
const byte BUTTON_Push_Right = 7;

bool BUTTON_Toggle_Left_lastState;
bool BUTTON_Toggle_Middle_lastState;
bool BUTTON_Toggle_Right_lastState;
bool BUTTON_Push_Left_lastState;
bool BUTTON_Push_Middle_lastState;
bool BUTTON_Push_Right_lastState;

//ROTARY COUNTERS
SimpleRotary rotaryLeft(10, 11, 12);
SimpleRotary rotaryMiddle(7, 8, 9);
SimpleRotary rotaryRight(4, 5, 6);
byte stateRotaryLeft = 0;
byte stateRotaryMiddle = 0;
byte stateRotaryRight = 0;


// the setup routine runs once when you press reset:
void setup() {

/*
 * Init Serial logging
 */
  //Serial.begin(9600);
  
/*
 * load saved preset
 */
  memValue = EEPROM.read(memAdd);

/*
 * init timer
 */
 lastCDMillis = millis();
 
  // initialize LEDS
  pinMode(LED_blink, OUTPUT);
  digitalWrite(LED_blink, HIGH); //Lit during init

  // initialize BUTTONS
  pinMode(BUTTON_Rotary_Left, INPUT_PULLUP);
  pinMode(BUTTON_Rotary_Middle, INPUT_PULLUP);
  pinMode(BUTTON_Rotary_Right, INPUT_PULLUP);

  //initialize shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, INPUT);

  //read shiftin buttons
  //Pulse the latch pin:
  //set it to 1 to collect parallel data
  digitalWrite(latchPin, 1);
  //set it to 1 to collect parallel data, wait
  delayMicroseconds(20);
  //set it to 0 to transmit data serially
  digitalWrite(latchPin, 0);

  //while the shift register is in serial mode
  //collect each shift register into a byte
  //the register attached to the chip comes in first
  shiftinValue = shiftIn(dataPin, clockPin);
  digitalWrite(clockPin, LOW);

  BUTTON_Toggle_Left_lastState = checkShiftInValue(shiftinValue, BUTTON_Toggle_Left);
  BUTTON_Toggle_Middle_lastState = checkShiftInValue(shiftinValue, BUTTON_Toggle_Middle);
  BUTTON_Toggle_Right_lastState = checkShiftInValue(shiftinValue, BUTTON_Toggle_Right);

  BUTTON_Push_Left_lastState = checkShiftInValue(shiftinValue, BUTTON_Push_Left);
  BUTTON_Push_Middle_lastState = checkShiftInValue(shiftinValue, BUTTON_Push_Middle);
  BUTTON_Push_Right_lastState = checkShiftInValue(shiftinValue, BUTTON_Push_Right);

  //Init rotary encoders
  rotaryLeft.setDebounceDelay(2);
  rotaryLeft.setErrorDelay(100);
  
  rotaryMiddle.setDebounceDelay(2);
  rotaryMiddle.setErrorDelay(100);
  
  rotaryRight.setDebounceDelay(2);
  rotaryRight.setErrorDelay(100);
  
  sendMessage("MacroBoard");

  refreshToggleStatus();
}

    
// the loop routine runs over and over again forever:
void loop() 
{
  //delay ? ok
  //delay(5);

  HIDIn = RawHID.recv(buffer, 0); // 0 timeout = do not wait
    
  //Message from the connected device
  if (Serial.available() > 0 || HIDIn > 0) 
  {
    if (Serial.available() > 0) message = Serial.readString();
    if (HIDIn > 0) message = String((char*)buffer);
    if (message == "MB?\n" || message == "MB?")
    {
      refreshToggleStatus();
    }
  }

  //Rotary Buttons
  if (digitalRead(BUTTON_Rotary_Left) == LOW && BUTTON_Rotary_Left_lastState == HIGH) // = pressed
  {
    //delay(50);
    sendMessage("Rotary_Left");
  }
  else if (digitalRead(BUTTON_Rotary_Middle) == LOW && BUTTON_Rotary_Middle_lastState == HIGH) // = pressed
  {
    //delay(50);
    sendMessage("Rotary_Middle");
  }
  else if (digitalRead(BUTTON_Rotary_Right) == LOW && BUTTON_Rotary_Right_lastState == HIGH) // = pressed
  {
    //delay(50);
    sendMessage("Rotary_Right");
  }

  BUTTON_Rotary_Left_lastState = digitalRead(BUTTON_Rotary_Left);
  BUTTON_Rotary_Middle_lastState = digitalRead(BUTTON_Rotary_Middle);
  BUTTON_Rotary_Right_lastState = digitalRead(BUTTON_Rotary_Right);

  if (millis() - lastCDMillis > coolDown)
  {
    lastCDMillis = millis();
    
    //read shiftin buttons
    //Pulse the latch pin:
    //set it to 1 to collect parallel data
    digitalWrite(latchPin, 1);
    //wait
    delayMicroseconds(20);
    //set it to 0 to transmit data serially
    digitalWrite(latchPin, 0);
  
    //while the shift register is in serial mode
    //collect each shift register into a byte
    //the register attached to the chip comes in first
    shiftinValue = shiftIn(dataPin, clockPin);
    digitalWrite(clockPin, LOW);
    //Serial.println(shiftinValue, BIN);
  
    //Button_select_1
    if (checkShiftInValue(shiftinValue, BUTTON_Toggle_Left) && !BUTTON_Toggle_Left_lastState)
    {
      sendMessage("Toggle_Left-");
    }
    if (!checkShiftInValue(shiftinValue, BUTTON_Toggle_Left) && BUTTON_Toggle_Left_lastState) 
    {
      sendMessage("Toggle_Left+");
    }
  
    //Button_select_2
    if (checkShiftInValue(shiftinValue, BUTTON_Toggle_Middle) && !BUTTON_Toggle_Middle_lastState) 
    {
      sendMessage("Toggle_Middle-");
    }
    if (!checkShiftInValue(shiftinValue, BUTTON_Toggle_Middle) && BUTTON_Toggle_Middle_lastState) 
    {
      sendMessage("Toggle_Middle+");
    }
  
    //Button_select_3
    if (checkShiftInValue(shiftinValue, BUTTON_Toggle_Right) && !BUTTON_Toggle_Right_lastState) 
    {
      sendMessage("Toggle_Right-");
    }
    if (!checkShiftInValue(shiftinValue, BUTTON_Toggle_Right) && BUTTON_Toggle_Right_lastState) 
    {
      sendMessage("Toggle_Right+");
    }
  
    //BUTTON_Push_Left
    if (checkShiftInValue(shiftinValue, BUTTON_Push_Left) && !BUTTON_Push_Left_lastState) 
    {
      sendMessage("Push_Left");
    }
  
    //Button_Push_Middle
    if (checkShiftInValue(shiftinValue, BUTTON_Push_Middle) && !BUTTON_Push_Middle_lastState) 
    {
      sendMessage("Push_Middle");
    }
  
    //BUTTON_Push_Right
    if (checkShiftInValue(shiftinValue, BUTTON_Push_Right) && !BUTTON_Push_Right_lastState) 
    {
      sendMessage("Push_Right");
    }
  
    BUTTON_Toggle_Left_lastState = checkShiftInValue(shiftinValue, BUTTON_Toggle_Left);
    BUTTON_Toggle_Middle_lastState = checkShiftInValue(shiftinValue, BUTTON_Toggle_Middle);
    BUTTON_Toggle_Right_lastState = checkShiftInValue(shiftinValue, BUTTON_Toggle_Right);
  
    BUTTON_Push_Left_lastState = checkShiftInValue(shiftinValue, BUTTON_Push_Left);
    BUTTON_Push_Middle_lastState = checkShiftInValue(shiftinValue, BUTTON_Push_Middle);
    BUTTON_Push_Right_lastState = checkShiftInValue(shiftinValue, BUTTON_Push_Right);
  
  }

  //rotary buttons

  //Rotary Left
  stateRotaryLeft = rotaryLeft.rotate();
  if ( stateRotaryLeft == 1 ) //Turned Clockwise
  {
    sendMessage("Rotary_Left+");
  }

  if ( stateRotaryLeft == 2 ) //Turned Counter-Clockwise
  {
    sendMessage("Rotary_Left-");
  }

  //Rotary Middle
  stateRotaryMiddle = rotaryMiddle.rotate();
  if ( stateRotaryMiddle == 1 ) //Turned Clockwise
  {
    sendMessage("Rotary_Middle+");
  }

  if ( stateRotaryMiddle == 2 ) //Turned Counter-Clockwise
  {
    sendMessage("Rotary_Middle-");
  }

  //Rotary Right
  stateRotaryRight = rotaryRight.rotate();
  if ( stateRotaryRight == 1 ) //Turned Clockwise
  {
    sendMessage("Rotary_Right+");
  }

  if ( stateRotaryRight == 2 ) //Turned Counter-Clockwise
  {
    sendMessage("Rotary_Right-");
  }
  
  //End Loop
  
  digitalWrite(LED_blink, LOW); // LED Blink default state
}

//Functions

//Compares a shiftIn input value with a variable
bool checkShiftInValue(byte _shiftinValue, byte _shiftInCheck) //compares shiftin data value with a button pin number
{
  return (_shiftinValue & (1 << (_shiftInCheck - 1)));
}

void refreshToggleStatus()
{
   //Button_select_1
    if (checkShiftInValue(shiftinValue, BUTTON_Toggle_Left))
    {
      sendMessage("Toggle_Left-");
    }
    if (!checkShiftInValue(shiftinValue, BUTTON_Toggle_Left)) 
    {
      sendMessage("Toggle_Left+");
    }
  
    //Button_select_2
    if (checkShiftInValue(shiftinValue, BUTTON_Toggle_Middle)) 
    {
      sendMessage("Toggle_Middle-");
    }
    if (!checkShiftInValue(shiftinValue, BUTTON_Toggle_Middle)) 
    {
      sendMessage("Toggle_Middle+");
    }
  
    //Button_select_3
    if (checkShiftInValue(shiftinValue, BUTTON_Toggle_Right)) 
    {
      sendMessage("Toggle_Right-");
    }
    if (!checkShiftInValue(shiftinValue, BUTTON_Toggle_Right)) 
    {
      sendMessage("Toggle_Right+");
    }
}

void sendKey(int key)
{
  Keyboard.press(key);
  Keyboard.release(key);
}
// Send Serial Data / Raw HID message
void sendMessage(String serialMessage) // sends a massage through serial
{
  if (serialMessage == "Rotary_Right") 
  {
    memValue = (memValue+1)%2;
    EEPROM.write(memAdd, memValue); //save mode
  }
  digitalWrite(LED_blink, HIGH); //Blinks when sending message
  //Serial.println(serialMessage);
  if (memValue == 1)
  {
    for (int i=0; i<64; i++) {
        buffer[i] = 0;
    }
    serialMessage.getBytes(buffer, 64);
    RawHID.send(buffer, 100);
  }
  else
  {
    if (serialMessage == "Rotary_Left") sendKey(KEY_MEDIA_MUTE);
    else if (serialMessage == "Rotary_Middle") sendKey(KEY_MEDIA_FAST_FORWARD);
    //else if (serialMessage == "Rotary_Right") sendKey();
    
    else if (serialMessage == "Toggle_Left-") sendKey(KEY_F13);
    else if (serialMessage == "Toggle_Left+") sendKey(KEY_F14);
    else if (serialMessage == "Toggle_Middle-") sendKey(KEY_F15);
    else if (serialMessage == "Toggle_Middle+") sendKey(KEY_F16);
    else if (serialMessage == "Toggle_Right-") sendKey(KEY_F17);
    else if (serialMessage == "Toggle_Right+") sendKey(KEY_F18);
    
    else if (serialMessage == "Push_Left") sendKey(KEY_MEDIA_PREV_TRACK);
    else if (serialMessage == "Push_Middle") sendKey(KEY_MEDIA_PLAY_PAUSE);
    else if (serialMessage == "Push_Right") sendKey(KEY_MEDIA_NEXT_TRACK);
    
    else if (serialMessage == "Rotary_Left-") sendKey(KEY_MEDIA_VOLUME_DEC);
    else if (serialMessage == "Rotary_Left+") sendKey(KEY_MEDIA_VOLUME_INC);
    else if (serialMessage == "Rotary_Middle-") sendKey(KEY_F19);
    else if (serialMessage == "Rotary_Middle+") sendKey(KEY_F20);
    else if (serialMessage == "Rotary_Right-") sendKey(KEY_F21);
    else if (serialMessage == "Rotary_Right+") sendKey(KEY_F22);
  }
}
